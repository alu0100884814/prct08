Node = Struct.new(:value, :next)
 
class Lista

    attr_reader :head, :tail
        
    def initialize()
        @head = nil
        @tail = nil
    end

    def empty()
        vacio = false
        if (@head == nil)
            vacio = true
        elsif
            vacio = false
        end
        return vacio
    end
 
    def insert(node)
        if (empty())
            @head = node
            @tail = @head
        elsif
            node.next = @head
            @head = node
        end
    end

    def insert_varios(nodes)
        i=0
        total=nodes.length
        while i<total do
           insert(nodes[i])
            i=i+1
        end
    end    
    
    def pop_front()
        if (empty())
            return nil
        elsif
           node = @head
           @head = @head.next
           node.next = nil
           return node.value
        end
    end    
end
