class Dieta

	attr_reader :titulo, :ingesta_diaria, :plato, :vct, :proteinas, :grasas, :hidratos

	def initialize(titulo, ingesta, plato, vct, proteinas, grasas, hidratos)
		@titulo = titulo
		@ingesta_diaria = ingesta
		@plato = plato
		@vct = vct
		@proteinas = proteinas
		@grasas = grasas
		@hidratos = hidratos
	end

	def cabecera()
		"#{@titulo} (#{@ingesta_diaria}%)\n"
	end

	def listado_platos()

		cadena = ""
		
		@plato.each do |x|
			cadena+= "- #{x.join(", ")}\n"
		end

		return cadena

	end

	def cierre()
		"V.C.T. | %  #{vct} kcal | #{proteinas}% - #{grasas}% - #{hidratos}%\n"
	end

	def to_s()
		cadena = ""
		cadena = cabecera + listado_platos + cierre
	end

end
