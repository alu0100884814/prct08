require "./lib/Directorio_de_trabajo_del_equipo/menu_dietetico.rb"
require "./lib/Directorio_de_trabajo_del_equipo/dieta.rb"

describe Node do
  
  before :all do
    @node1 = Node.new(1,nil)
    @node2 = Node.new(2,@node1)
  end

context Node do
    it "Debe existir el Nodo de la lista " do
       expect(@node1.value).to eq(1)
       expect(@node2.value).to eq(2)
       expect(@node1.next).to eq(nil)
       expect(@node2.next).to eq(@node1)
     end
  end
end

describe Lista do
  
  before :each do
    
    @test=Dieta.new("Desayuno", 40, [["Tostadas", "2 unidades", "200 gramos"],["Zumo de naranja","1 unidad","200 centilitros"]], 30,20,15,10)
end

describe "# almacenamiento de los valores de las variables"
	it "Se almacena correctamente el titulo" do
		@test.titulo.should eq("Desayuno")
	end
	it "Se almacena correctamente la ingesta diaria" do
		@test.ingesta_diaria.should eq(40)
	end

	it "Se almacena correctamente el nombre del plato" do
		@test.plato[0][0].should eq("Tostadas")
	end
	it "Se almacena correctamente la porcion" do
		@test.plato[0][1].should eq("2 unidades")
	end

	it "Se almacena correctamente la ingesta en gramos" do
		@test.plato[0][2].should eq("200 gramos")
	end
	it "Se almacena correctamente la VCT" do
		@test.vct.should eq(30)
	end

	it "Se almacena correctamente las proteinas" do
		@test.proteinas.should eq(20)
	end
	it "Se almacena correctamente las grasas" do
		@test.grasas.should eq(15)
		end

	it "Se almacena correctamente los hidratos de carbono" do
		@test.hidratos.should eq(10)
	end

	before :each do

		@test2=Dieta.new("Almuerzo", 60, [["Bistec de cerdo", "2 unidades", "200 gramos"],["Arroz","1 porcion","300gramos"]], 40,30,25,15)

	end	

	describe "# almacenamiento de los valores de las variables"
	it "Se almacena correctamente el titulo" do
		@test2.titulo.should eq("Almuerzo")
	end
	it "Se almacena correctamente la ingesta diaria" do
		@test2.ingesta_diaria.should eq(60)
	end

	it "Se almacena correctamente el nombre del plato" do
		@test2.plato[0][0].should eq("Bistec de cerdo")
	end
	it "Se almacena correctamente la porcion" do
		@test2.plato[0][1].should eq("2 unidades")
	end

	it "Se almacena correctamente la ingesta en gramos" do
		@test2.plato[0][2].should eq("200 gramos")
	end
	it "Se almacena correctamente la VCT" do
		@test2.vct.should eq(40)
	end

	it "Se almacena correctamente las proteinas" do
		@test2.proteinas.should eq(30)
	end
	it "Se almacena correctamente las grasas" do
		@test2.grasas.should eq(25)
		end

	it "Se almacena correctamente los hidratos de carbono" do
		@test2.hidratos.should eq(15)
	end


	@node3 = Node.new(@test, @test2)
	@node4 = Node.new(@test2, nil)
    	
end

describe Lista do		

	before :all do
		@Lista = Lista.new()
	end

	context Lista do
			
	it "Se puede comprobar si la lista está vacía" do
      expect(@Lista.empty()).to eq (true)
    end
    
    it "Se puede insertar un elemento" do
      @Lista.insert(@node3)
      expect(@Lista.head).to eq(@node3)
    end
	
	it "Se pueden insertar varios elementos" do
		@Lista.insert_varios([@node3, @node4])
	end 
	
   it "Se puede extraer el primer elemento de la lista" do
		@node5 = Node.new(5,0)
      	@Lista.insert(@node5)
      	expect(@Lista.pop_front().to_s).to eq("5")
   end    
  
  end
   
end



