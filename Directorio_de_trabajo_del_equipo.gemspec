# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'Directorio_de_trabajo_del_equipo/version'

Gem::Specification.new do |spec|
  spec.name          = "Directorio_de_trabajo_del_equipo"
  spec.version       = DirectorioDeTrabajoDelEquipo::VERSION
  spec.authors       = ["N. Ibrahim Hernández Jorge"]
  spec.email         = ["alu0100884814@ull.edu.es"]

  spec.summary       = %q{Menú dietético en Listas Enlazadas.}
  spec.description   = %q{La creacion de una clase Lista que contiene Nodos (Una estructura con cabeza y cola).}
  spec.homepage      = "https://github.com/ULL-ESIT-LPP-1617/menu-dietetico-alu0100884814"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
